import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { MovieCardComponent } from './movies/movie-card/movie-card.component';

const routes: Routes = [
  {
    path: '',
    component: MoviesComponent,
    pathMatch: 'full',
  },
  {
    path: ':key',
    component: MovieCardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
