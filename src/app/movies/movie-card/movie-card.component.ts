import { Component, OnInit, Input } from '@angular/core';
import { MovieService } from '../movie.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  movie: any;
  matchingMovies: any[];

  constructor(private movieService: MovieService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      window.scrollTo(0, 0);

      this.movie = this.movieService.getMovieByKey(params['key']);
      this.matchingMovies = this.movieService.getMatchingMovies(this.movie);
    });
  }
}
