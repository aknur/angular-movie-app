import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss'],
})
export class MovieItemComponent implements OnInit {
  @Input() movie: any;
  posterPath = 'assets/images/movie-covers/';

  constructor() {}

  ngOnInit(): void {
    this.posterPath += this.movie.img;
  }
}
