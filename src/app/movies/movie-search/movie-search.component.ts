import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.scss'],
})
export class MovieSearchComponent implements OnInit {
  searchForm: FormGroup;
  showBackButton = false;

  constructor(private router: Router, private route: ActivatedRoute, private location: Location) {}

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      search: new FormControl(null),
    });

    this.route.queryParams.subscribe((params: Params) => {
      const { search } = params;

      this.searchForm.setValue({
        search: search ? search : '',
      });
      this.showBackButton = search && true;
    });
  }

  onSubmit() {
    const search = this.searchForm.get('search').value;
    this.router.navigate(['/'], { queryParams: { search } });
  }

  goBack() {
    this.location.back();
  }
}
