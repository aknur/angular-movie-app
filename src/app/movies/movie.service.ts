import { Injectable } from '@angular/core';

import { movies } from './movie.mock-data';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  private movies: any[] = movies;

  constructor() {}

  getAllMovies() {
    return movies.slice();
  }

  getMovieByKey(key: string) {
    return this.movies.find((movie: any) => movie.key === key);
  }

  searchMovie(text: string) {
    const pattern = text
      .split('')
      .map((x) => {
        return `(?=.*${x})`;
      })
      .join('');
    const regex = new RegExp(pattern, 'gi');
    return this.movies.filter((movie: any) => movie.name.match(regex));
  }

  getMoviesByGenre(genre: string) {
    return this.movies.filter((movie: any) => movie.genres.includes(genre));
  }

  getMatchingMovies(movie: any) {
    let matchingMovies = [];

    for (let genre of movie.genres) {
      matchingMovies.push(...this.getMoviesByGenre(genre));
    }

    matchingMovies = Array.from(new Set(matchingMovies));

    return matchingMovies;
  }
}
