import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { MovieItemComponent } from './movies/movies-list/movie-item/movie-item.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieCardComponent } from './movies/movie-card/movie-card.component';
import { MovieSearchComponent } from './movies/movie-search/movie-search.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesListComponent,
    MovieItemComponent,
    MoviesComponent,
    MovieCardComponent,
    MovieSearchComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
